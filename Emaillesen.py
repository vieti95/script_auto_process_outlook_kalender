import win32com.client as client
import datetime
import pytz
import json

def create_calendar_entry(outlook, employee_name, start_date, end_date):
    cal_item = outlook.CreateItem(1)
    cal_item.Subject = employee_name
    cal_item.Body = "Vacation"
    cal_item.Importance = 1
    cal_item.Start = start_date
    cal_item.End = end_date
    cal_item.Display()
    inspector = cal_item.GetInspector
    inspector.Close(1)  # Fenster schließen, ohne Änderungen zu speichern
    cal_item.Save()

def find_and_delete_calendar_entry(calendar_folder, name_from_json):
    # Durchsuche die Kalenderobjekte nach den bestimmten Betreffnamen
    for calendar_item in calendar_folder.Items:
        if calendar_item.Subject == name_from_json:  # Prüfe, ob der Betreff übereinstimmt
            # Lösche den Kalendereintrag, wenn der Betreff übereinstimmt
            calendar_item.Delete()
            print("Kalendereintrag erfolgreich gelöscht.")
            return True  # Rückgabe, dass der Eintrag gefunden und gelöscht wurde

    print("Keine übereinstimmenden Kalendereinträge gefunden.")
    return False  # Rückgabe, dass kein Eintrag gefunden wurde




# Lese die Namen aus der JSON-Datei
with open('employees.json') as f:
    names_data = json.load(f)

# Hier wird angenommen, dass die JSON-Datei ein Array von Namen enthält
names = [name.strip() for name in names_data['name']]


# Outlook-Anwendung initialisieren
outlook = client.Dispatch('outlook.application')
namespace = outlook.GetNamespace("MAPI")

# Zugriff auf den Posteingang und den Kalenderordner
inbox = outlook.Session.GetDefaultFolder(6)
calendar_folder = namespace.GetDefaultFolder(9)  # Kalenderordner

# Zugriff auf die gesendeten E-Mails
sent_items = outlook.Session.GetDefaultFolder(5).Items
sent_items.IncludeRecurrences = True


# Überprüfen, ob ein Kalendereintrag mit dem bestimmten Betreff existiert
calendar_items = calendar_folder.Items
calendar_entry_exists = any(item.Subject in names for item in calendar_items)


if not calendar_entry_exists:
    subject_to_find = "Action required - Absence Request - A request is awaiting your approval"
    target_email = None
    latest_received_time = None

    local_tz = pytz.timezone('Europe/Berlin')

    for email in inbox.Items:
        if email.Subject == subject_to_find:
            email_received_time = email.ReceivedTime.replace(tzinfo=None)
            email_received_time = local_tz.localize(email_received_time)
            if latest_received_time is None or email_received_time > latest_received_time:
                target_email = email
                latest_received_time = email_received_time

    if target_email:
        employee_name = ""
        start_date = ""
        end_date = ""

        val1 = target_email.body.splitlines()  # Inhalt der E-Mail
        for line in val1:
            if "Employee name:" in line:
                employee_name = line.replace("Employee name:", "").strip()
            if "Start date:" in line:
                start_date = line.replace("Start date:", "").strip()
            if "End date:" in line:
                end_date = line.replace("End date:", "").strip()

        create_calendar_entry(outlook, employee_name, start_date, end_date)

current_date = datetime.datetime.now().date()

if calendar_entry_exists:
    current = datetime.datetime.now().date()

    if names:
        for name_from_json in names:
            found = any(name_from_json in email.Subject for email in inbox.Items)
            if found:
                subject_to_find = f"Cancelled - Absence request - The request for {name_from_json} has been cancelled."
                print(f"Der Name '{name_from_json}' wurde in der E-Mail gefunden.")

                # Durchsuche die E-Mails im Posteingang
                for email in inbox.Items:
                    if email.Subject == subject_to_find and email.SentOn.date() == current:
                        print(f"Die zugehörige E-Mail für '{name_from_json}' wurde gefunden und ist aktuell.")
                        # Durchsuche die Kalendereinträge und lösche den entsprechenden Eintrag
                        for calendar_item in calendar_folder.Items:
                            if calendar_item.Subject == name_from_json:
                                calendar_item.Delete()
                                print(f"Kalendereintrag für '{name_from_json}' erfolgreich gelöscht.")
                                break
                        else:
                            print(f"Kein Kalendereintrag für '{name_from_json}' gefunden.")
                        break  # Aus der Schleife für die E-Mails ausbrechen, wenn das aktuelle Datum gefunden wurde
                else:
                    print("Die zugehörige E-Mail wurde nicht gefunden oder ist nicht aktuell.")

    subject_to_find_sent = "Denied - Absence Request - Your request has been denied"
    current_date = datetime.datetime.now().date()

    for sent_email in sent_items:
        if sent_email.SentOn.date() == current_date and sent_email.Subject == subject_to_find_sent:
            # E-Mail gefunden, die heute gesendet wurde und den gewünschten Betreff hat
            for calendar_item in calendar_folder.Items:
                # Überprüfe, ob der Kalendereintrag in der Liste von Namen enthalten ist
                if calendar_item.Subject in names:
                    calendar_item.Delete()
                    print("Kalendereintrag erfolgreich gelöscht.")
                    break  # Beende die Schleife nach dem Löschen des Eintrags
            break  # Beende die äußere Schleife nach dem Finden des gesuchten Betreffs
        else:
            print("Der gesendete Betreff wurde nicht gefunden oder es gab keine übereinstimmenden Kalendereinträge.")